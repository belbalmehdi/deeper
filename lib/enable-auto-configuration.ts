import path from 'path'
import fs from 'fs'
import { Instantiable } from './spring-boot-application'
import { container } from 'tsyringe'
import { Environment } from './environment'

export function EnableAutoConfiguration (instantiable: Instantiable) {
  const defaultConfigFile = path.join(process.cwd(), 'src', 'resources', 'application.json')
  fs.accessSync(defaultConfigFile)
  const rawConfig: string = fs.readFileSync(defaultConfigFile, { encoding: 'utf8' })
  container.registerSingleton(Environment)
  const env = container.resolve(Environment)
  // @ts-ignore
  env._config = JSON.parse(rawConfig)
}

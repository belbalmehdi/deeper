export class NoSuchBeanDefinitionException extends Error {
  constructor (message: string) {
    super(message)
  }
}

export interface CommandLineRunner {
  run (args: string[]): void
}

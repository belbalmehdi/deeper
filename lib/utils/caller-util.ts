// eslint-disable-next-line no-undef
export type Callee = NodeJS.CallSite
export function getStack (): Callee[] {
  const original = Error.prepareStackTrace
  Error.prepareStackTrace = function (_, stack): Callee[] {
    return stack
  }
  const stack: Callee[] = (new Error().stack) as unknown as Callee[]
  Error.prepareStackTrace = original
  stack.shift()
  return stack
}

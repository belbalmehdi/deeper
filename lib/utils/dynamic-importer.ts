import path from 'path'

export function dynamicImport (dirname: string, filename: string): Promise<any> {
  const relativeDirPath: string = path.relative(__dirname, dirname)
  const modulePath = path.join(relativeDirPath, filename)
  return import(modulePath)
}

import { Instantiable } from './spring-boot-application'
import path from 'path'
import * as fs from 'fs'
import { container } from 'tsyringe'
import { Callee, getStack } from './utils/caller-util'
import { dynamicImport } from './utils/dynamic-importer'
import { EnableAutoConfiguration } from './enable-auto-configuration'
import { handleAutowiring } from './decorators/autowired'
import { Component } from './decorators/component'
import { WebContext } from './web/web-context'

export class SpringApplication {
  static run (mainClass: Instantiable, args: string[]) {
    EnableAutoConfiguration(mainClass)
    const stack: Callee[] = getStack()
    const mainFile: string = stack[1].getFileName()!!
    const filename: string = path.basename(mainFile)
    const dirname: string = path.dirname(mainFile)
    Promise.all(fs.readdirSync(dirname)
      .filter(file => file !== filename)
      .map(file => dynamicImport(dirname, file))
    )
      .then((modules) => {
        handleAutowiring(mainClass)
        modules.flatMap(module => Object.values(module))
          .filter((ctor: any) => Reflect.hasMetadata(Component, ctor))
          .forEach(handleAutowiring)
        container.resolve(WebContext)?.run()
        container.resolve(mainClass)?.run(process.argv)
      })
  }
}

import { RouterHandlers } from '../decorators/rest-controller'
import { Component } from '../decorators/component'

@Component()
export class ControllersContainer {
  readonly container: Map<RegExp | string, RouterHandlers> = new Map<RegExp | string, RouterHandlers>()

  add (path: RegExp | string, handler: RouterHandlers) {
    this.container.set(path, handler)
  }

  get routes (): (string | RegExp)[] {
    return [...this.container.keys()]
  }

  getRoute (path: string | RegExp): RouterHandlers {
    return this.container.get(path)!!
  }
}

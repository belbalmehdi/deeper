import { Component } from '../decorators/component'
import express, { Express, IRoute, Request, Response, Router } from 'express'
import { Environment } from '../environment'
import { ControllersContainer } from './controllers-container'
import { RequestHandler, RouterHandlers } from '../decorators/rest-controller'
import { HttpMethod } from './http-method'

@Component()
export class WebContext {
  private expressApp: Express
  readonly serverPort: number

  constructor (private env: Environment, private controllerContainer: ControllersContainer) {
    this.expressApp = express()
    this.serverPort = this.env.getProperty('PORT') || 80
    this.createRouters()
  }

  createRouters () {
    this.controllerContainer.routes.forEach(
      (route) => {
        const routeData: RouterHandlers = this.controllerContainer.getRoute(route)
        const router: Router = Router()
        routeData.handlers.forEach(handler => this.createRouteHandler(router, handler))
        this.expressApp.use(routeData.routerPath, router)
      }
    )
  }

  private createRouteHandler (router: Router, handler: RequestHandler) {
    const callback = (req: Request, res: Response) => {
      const result: any = handler.handler()
      if (result instanceof Object) res.json(result)
      else res.end(result)
    }

    const route: IRoute = router.route(handler.methodPath)
    switch (handler.method) {
      case HttpMethod.GET: route.get(callback); break
      case HttpMethod.POST: route.post(callback); break
      case HttpMethod.PUT: route.put(callback); break
      case HttpMethod.DELETE: route.delete(callback); break
      case HttpMethod.PATCH: route.patch(callback); break
      case HttpMethod.TRACE: route.trace(callback); break
      case HttpMethod.UPDATE: route.put(callback); break
      default: route.all(callback)
    }
  }

  run () {
    this.expressApp.listen(this.serverPort, () => {
      console.log(`server listening on port ${this.serverPort}`)
    })
  }
}

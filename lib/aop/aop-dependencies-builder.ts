import { container } from 'tsyringe'
import { Instantiable } from '../spring-boot-application'
import { INJECT_ARGUMENTS, INJECT_RETURN } from './aop-symbols'

export function aopDependenciesBuilder (args: any[], result: any, dependencies: (Instantiable | Symbol)[]) {
  return dependencies.map(dep => {
    return dep === INJECT_ARGUMENTS ? args : (dep === INJECT_RETURN ? result : container.resolve(dep as Instantiable))
  })
}

import { aopDependenciesBuilder } from './aop-dependencies-builder'

export function Before (callback: Function, dependencies: any[] = []) {
  return function (object: any, method: string, descriptor: PropertyDescriptor) {
    const org = descriptor.value
    descriptor.value = function (...args: any[]) {
      callback.apply(this, aopDependenciesBuilder(args, undefined, dependencies))
      return org.apply(this, args)
    }
  }
}

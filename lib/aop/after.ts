import { aopDependenciesBuilder } from './aop-dependencies-builder'

export function After (callback: Function, dependencies: any[] = []) {
  return function (object: any, method: string, descriptor: PropertyDescriptor) {
    const org = descriptor.value
    descriptor.value = function (...args: any[]) {
      const res: any = org.apply(this, args)
      callback.apply(this, aopDependenciesBuilder(args, res, dependencies))
      return res
    }
  }
}

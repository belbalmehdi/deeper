import { Lifecycle, singleton } from 'tsyringe'
import { Component, ComponentConf } from './component'

export function Service (config: ComponentConf = { scope: Lifecycle.Singleton }) {
  return function (instantiable: any) {
    Reflect.defineMetadata(Service, config, instantiable)
    if (!config.scope || config.scope === Lifecycle.Singleton) {
      Component.call(this, config).call(this, instantiable)
    }
  }
}

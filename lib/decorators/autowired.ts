import { NoSuchBeanDefinitionException } from '../exceptions/no-such-bean-definition-exception'
import { container } from 'tsyringe'

export function Autowired (instantiable: any, prop: string, descriptor?: PropertyDescriptor) {
  const metadata = Reflect.hasMetadata(Autowired, instantiable.constructor)
    ? Reflect.getMetadata(Autowired, instantiable.constructor)
    : {}

  Reflect.defineMetadata(Autowired, {
    ...metadata,
    [prop]: instantiable
  }, instantiable.constructor)
}

export function handleAutowiring (instantiable: any) {
  const autowireProps = Reflect.getOwnMetadata(Autowired, instantiable)
  for (const prop in autowireProps) {
    const instance = autowireProps[prop]
    const dependencyType = Reflect.getOwnMetadata('design:type', instance, prop)
    const dependency = container.resolve(dependencyType)
    if (!dependency) {
      throw new NoSuchBeanDefinitionException('no bean definition for type ' + dependencyType.name)
    }
    instance[prop] = dependency
  }
}

import { HttpMethod } from '../web/http-method'

export interface RequestMappingOptions {
  path: string
  method: HttpMethod
}

export function RequestMapping (options: RequestMappingOptions | string) {
  return function (instantiable: any, method: string, descriptor: PropertyDescriptor) {
    const metadata = options instanceof String ? { path: options, method: HttpMethod.GET } : options
    Reflect.defineMetadata(RequestMapping, metadata, instantiable.constructor.prototype[method])
  }
}

import { RequestMapping, RequestMappingOptions } from './request-mapping'
import { Component } from './component'
import { ControllersContainer } from '../web/controllers-container'
import { HttpMethod } from '../web/http-method'
import { Instantiable } from '../spring-boot-application'
import { container } from 'tsyringe'

export interface RequestHandler {
  routerPath: string | RegExp,
  methodPath: string | RegExp,
  method: HttpMethod,
  handler: Function
}

export interface RouterHandlers {
  routerPath: string | RegExp,
  handlers: RequestHandler[]
}

export function RestController (path: string | RegExp = '') {
  return function (instantiable: Instantiable) {
    Reflect.defineMetadata(RestController, path, instantiable)
    Component.call(this).call(this, instantiable)
    const controller = container.resolve(instantiable)
    const requestHandlers: RequestHandler[] = Object.getOwnPropertyNames(controller.constructor.prototype)
      .filter((method: string) => {
        return controller.constructor.prototype[method] instanceof Function &&
          Reflect.hasMetadata(RequestMapping, controller.constructor.prototype[method])
      }).map((method: string) => {
        const methodMetadata: RequestMappingOptions = Reflect.getMetadata(RequestMapping, controller.constructor.prototype[method])
        return {
          routerPath: path,
          methodPath: methodMetadata.path,
          method: methodMetadata.method,
          handler: controller[method].bind(controller)
        }
      })
    const controllersContainer: ControllersContainer = container.resolve(ControllersContainer)
    controllersContainer.add(path, { routerPath: path, handlers: requestHandlers })
  }
}

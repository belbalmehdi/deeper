import { Lifecycle, singleton } from 'tsyringe'
import { handleAutowiring } from './autowired'

export interface ComponentConf {
  name?: string,
  scope?: Lifecycle
}

export function Component (config: ComponentConf = { scope: Lifecycle.Singleton }) {
  return function (instantiable: any) {
    Reflect.defineMetadata(Component, config, instantiable)
    if (!config.scope || config.scope === Lifecycle.Singleton) {
      singleton.call(this).call(this, instantiable)
    }
  }
}

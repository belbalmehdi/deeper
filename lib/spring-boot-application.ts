import { container } from 'tsyringe'
import { handleAutowiring } from './decorators/autowired'
import { Component } from './decorators/component'

type Any = any

export interface Instantiable extends Any {
  new (...args: any[]): any
}

export class NoStaticMainMethodException extends Error {
  constructor (msg: string) {
    super(msg)
  }
}

export function SpringBootApplication (instantiable: Instantiable) {
  if (!instantiable.main) {
    throw new NoStaticMainMethodException(`No main declared on class ${instantiable.name}`)
  }
  Component.call(this).call(this, instantiable)
  instantiable.main(process.argv)
}

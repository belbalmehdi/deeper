import { Configuration } from './application-configuration'

export class Environment {
  private _config: Configuration = {}

  getProperty (property: string): any {
    return this._config[property]
  }
}

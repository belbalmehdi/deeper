import { Instantiable, NoMainException, SpringBootApplication } from '../../../lib/spring-boot-application'

describe('springBootApplication Decorator Test', () => {
  let TestApp: Instantiable
  beforeEach(() => {
    TestApp = class {
    }
  })

  test('should throw NoMainException static main method not found', () => {
    expect(() => SpringBootApplication(TestApp)).toThrow(NoMainException)
  })

  test('should call main method', () => {
    const spy = jest.fn()
    TestApp.main = spy
    SpringBootApplication(TestApp)
    expect(spy).toBeCalledTimes(1)
  })
})

import { HttpMethod } from '../../lib/web/http-method'
import { RequestMapping } from '../../lib/decorators/request-mapping'
import { UserService } from './user-service'
import { RestController } from '../../lib/decorators/rest-controller'
import { Autowired } from '../../lib/decorators/autowired'

@RestController('/api')
export class HomeController {
  @Autowired
  private userService!: UserService

  @RequestMapping({
    path: '/user',
    method: HttpMethod.GET
  })
  getUser () {
    return this.userService.getUser('shikki')
  }

  @RequestMapping({
    path: '/player',
    method: HttpMethod.GET
  })
  getPlayer (): string {
    return 'shanks'
  }
}

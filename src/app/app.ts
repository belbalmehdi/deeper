import 'reflect-metadata'
import { ControllersContainer } from '../../lib/web/controllers-container'
import { CommandLineRunner } from '../../lib/command-line-runner'
import { SpringApplication } from '../../lib/spring-application'
import { SpringBootApplication } from '../../lib/spring-boot-application'
import { Autowired } from '../../lib/decorators/autowired'
import { Environment } from '../../lib/environment'

@SpringBootApplication
export class App implements CommandLineRunner {
  @Autowired
  private controllerContainer!: ControllersContainer

  @Autowired
  private env!: Environment

  static main (args: string[]): void {
    SpringApplication.run(App, args)
  }

  run (args: string[]): void {
  }
}

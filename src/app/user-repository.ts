import { Repository } from '../../lib/decorators/repository'

export abstract class DomainRepository {
  abstract fetchUser (): any
}

@Repository()
export class UserRepository extends DomainRepository {
  fetchUser (): any {
    return {
      username: 'shikki',
      password: 'pixel',
      isAdmin: true
    }
  }
}

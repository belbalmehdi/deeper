import { UserRepository } from './user-repository'
import { Service } from '../../lib/decorators/service'
import { Before } from '../../lib/aop/before'
import { INJECT_ARGUMENTS, INJECT_RETURN } from '../../lib/aop/aop-symbols'
import { After } from '../../lib/aop/after'

@Service()
export class UserService {
  constructor (private userRepo: UserRepository) {
  }

  @Before(verifyRole, [UserRepository, INJECT_ARGUMENTS])
  @After(logRepositoryResponse, [INJECT_RETURN])
  getUser (name: string): any {
    return this.userRepo.fetchUser()
  }
}

function verifyRole (userRepo: UserRepository, args: any[]) {
  console.log(args)
  if (!userRepo.fetchUser().isAdmin) {
    throw new Error('user is not and admin')
  }
}

function logRepositoryResponse (data: any) {
  console.log(data)
}

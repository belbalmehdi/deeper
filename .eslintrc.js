module.exports = {
  root: true,
  env: {
    es2021: true,
    node: true,
    jest: true
  },
  extends: [
    'standard'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module'
  },
  plugins: [
    '@typescript-eslint'
  ],
  rules: {
    'no-console': 'off',
    'new-cap': 'off',
    'no-use-before-define': 'error',
    'no-extend-native': 'off',
    'no-unused-vars': 'off',
    'no-useless-constructor': 'off',
  }
}
